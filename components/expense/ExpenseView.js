import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;

export default class ExpenseView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.textWhite}>{this.props.expense.item}</Text>
        <View style={styles.priceContainer}>
          <Text style={styles.textBold}>{this.props.expense.price}</Text>
          <Text> RON</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textBold: {
    fontFamily: 'Roboto',
    fontWeight: 'bold'
  },
  textBlack: {
    color: 'black'
  },
  textWhite: {
    color: 'white'
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 0,
    paddingLeft: 10,
    backgroundColor: '#595959',
    marginBottom: 2,
    borderRadius: 5,
    width: WIDTH - 50,
    overflow: 'hidden'
  },
  priceContainer: {
    height: '100%',
    padding: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#efd300',
    flexDirection: 'row',
    minWidth: 70
  }
});
