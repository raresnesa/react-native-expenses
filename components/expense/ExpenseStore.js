import React, { Component } from 'react';
import { getLogger } from '../../core/utils';
import { httpApiUrl, wsApiUrl } from '../../core/api';
import { Provider } from './context';

const log = getLogger('ExpenseStore');

export default class ExpenseStore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      expenses: null,
      issue: null
    };
    this.sorter = (e1, e2) => e2.price - e1.price;
  }

  componentDidMount = () => {
    this.loadNotes();
    this.connectWs();
  };

  componentWillUnmount = () => {
    this.disconnectWs();
  };

  loadNotes = async () => {
    this.setState({ isLoading: true, issue: null });
    fetch(`${httpApiUrl}/expense`)
      .then(response => response.json())
      .then(expenses =>
        this.setState({
          isLoading: false,
          expenses: expenses.sort(this.sorter)
        })
      )
      .catch(error => this.setState({ isLoading: false, issue: error }));
  };

  connectWs = () => {
    const ws = new WebSocket(wsApiUrl);
    ws.onopen = () => {
      log('Websocket connection opened');
      ws.send('1');
    };

    ws.onerror = issue => {
      log('Websocket error');
      this.setState({ issue });
    };

    ws.onclose = () => {
      log('Websocket connection closed');
    };

    ws.onmessage = message => {
      const event = JSON.parse(message.data);
      log(`new event : ${event.event}`);
      switch (event.event) {
        case 'expense/created':
          this.handleExpenseCreated(event.payload);
          break;
        case 'expense/updated':
          this.handleExpenseUpdated(event.payload);
          break;
        case 'expense/deleted':
          this.handleExpenseDeleted(event.payload);
          break;
      }
    };
  };

  handleExpenseCreated(expense) {
    this.setState({
      expenses: this.state.expenses.concat([expense]).sort(this.sorter)
    });
  }

  handleExpenseUpdated(expense) {
    this.setState({
      expenses: this.state.expenses
        .map(exp => (exp._id === expense._id ? expense : exp))
        .sort(this.sorter)
    });
  }

  handleExpenseDeleted(expense) {
    this.setState({
      expenses: this.state.expenses
        .filter(exp => exp._id !== expense._id)
        .sort(this.sorter)
    });
  }

  sortList() {}

  render() {
    return (
      <Provider
        value={{
          ...this.state
        }}
      >
        {this.props.children}
      </Provider>
    );
  }
}
