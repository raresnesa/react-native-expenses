import React from 'react';
import ExpenseStore from './ExpenseStore';
import ExpenseList from './ExpenseList';

export default () => (
  <ExpenseStore>
    <ExpenseList />
  </ExpenseStore>
);
