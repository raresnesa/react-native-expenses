import React, { Component } from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  StyleSheet,
  Dimensions
} from 'react-native';
import { getLogger, issueToText } from '../../core/utils';
import utilStyles from '../../core/styles';
import ExpenseView from './ExpenseView';
import { Consumer } from './context';

const WIDTH = Dimensions.get('window').width;

const log = getLogger('ExpenseList');

export default class ExpenseList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Consumer>
        {({ isLoading, issue, expenses }) => (
          <View>
            <ActivityIndicator
              animating={isLoading}
              style={utilStyles.activityIndicator}
              size="large"
            />
            {issue && <Text>{JSON.stringify(issue)}</Text>}

            <Text style={styles.header}>My expenses</Text>
            <View style={styles.hr} />

            {expenses &&
              expenses.map(expense => (
                <ExpenseView key={expense._id} expense={expense} />
              ))}
          </View>
        )}
      </Consumer>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    fontSize: 25,
    marginBottom: 0
  },
  hr: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    width: WIDTH - 50,
    marginTop: 20,
    marginBottom: 20
  }
});
