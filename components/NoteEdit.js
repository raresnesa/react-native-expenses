import React from 'react';
import { StyleSheet, Text, View, Dimensions, Button } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

const WIDTH = Dimensions.get('window').width;

export default class NoteEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  handleOnPress() {
    this.props.onNoteSubmit({
      text: this.state.text
    });

    this.setState({
      text: ''
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          placeholder="Enter a note"
          value={this.state.text}
          onChangeText={text => this.setState({ text })}
        />
        <Button
          style={styles.buttonS}
          onPress={() => this.handleOnPress()}
          title="Add note"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textInput: {
    width: WIDTH - 40,
    height: 60
  },
  buttonS: {
    width: WIDTH - 40,
    backgroundColor: '#d62a2a',
    marginTop: 20,
    marginBottom: 20
  }
});
