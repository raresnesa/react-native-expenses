import { StyleSheet } from 'react-native';

const utilStyles = StyleSheet.create({
  content: {
    marginTop: 70,
    flex: 1
  },
  activityIndicator: {
    height: 50
  }
});

export default utilStyles;
