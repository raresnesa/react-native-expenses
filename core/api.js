export const apiUrl = '192.168.1.5:3000/api';
export const httpApiUrl = `http://${apiUrl}`;
export const wsApiUrl = `ws://${apiUrl}`;
export const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json'
};
